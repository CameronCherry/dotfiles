#
# ~/.bashrc
#

export PATH=$PATH:~/.local/bin

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

alias ls='ls --color=auto'
PS1='[\u@\h \W]\$ '

### RANDOM COLOUR SCIPT ###
colorscript random

alias config='/usr/bin/git --git-dir=/home/cameron/dotfiles/ --work-tree=/home/cameron'

shopt -s checkwinsize
