#!/bin/sh
redshift-gtk -c ~/.config/redshift.conf &
xbindkeys -f ~/.xbindkeysrc &
nitrogen --restore &
nm-applet &
dunst &
