# Copyright (c) 2010 Aldo Cortesi
# Copyright (c) 2010, 2014 dequis
# Copyright (c) 2012 Randall Ma
# Copyright (c) 2012-2014 Tycho Andersen
# Copyright (c) 2012 Craig Barnes
# Copyright (c) 2013 horsik
# Copyright (c) 2013 Tao Sauvage
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

import os
import subprocess
from libqtile.config import Key, Screen, Group, Drag, Click
from libqtile.lazy import lazy
from libqtile import layout, bar, widget, hook

from typing import List  # noqa: F401


mod = "mod4"
qConfig = "/home/cameron/.config/qtile/config.py"
myTerm = "termite"
termConfig = "/home/cameron/.config/termite/config"


keys = [
    # Switch between windows in current stack pane
    Key([mod], "k", lazy.layout.down()),
    Key([mod], "j", lazy.layout.up()),

    # Move windows up or down in current stack
    Key([mod, "control"], "k", lazy.layout.shuffle_down()),
    Key([mod, "control"], "j", lazy.layout.shuffle_up()),

    # Switch window focus to other pane(s) of stack
    Key([mod], "space", lazy.layout.next()),

    # Swap panes of split stack
    Key([mod, "shift"], "space", lazy.layout.rotate()),

    # Toggle between split and unsplit sides of stack.
    # Split = all windows displayed
    # Unsplit = 1 window displayed, like Max layout, but still with
    # multiple stack panes
    Key([mod, "shift"], "Return", lazy.layout.toggle_split()),
    
    # Shortcuts for various programs
    Key([mod], "Return", lazy.spawn(f"{myTerm} --config={termConfig}")),
    Key([mod], "f", lazy.spawn("firefox")),
    Key([mod, "shift"], "f", lazy.spawn("firefox --private-window")),
    Key([mod], "r", lazy.spawn(f"{myTerm} --config={termConfig} --exec=ranger")),
    Key([mod], "d", lazy.spawn("dmenu_run -p 'Run: ' -fn 'CaskaydiaCove Nerd Font' -sb '#d27c02' -l 7")),
    Key([mod], "c", lazy.spawn(f"{myTerm} --config={termConfig} --exec=\"vim {qConfig}\"")),
    Key([mod], "s", lazy.spawn("spyder")),
    Key(["control","shift"], "Escape", lazy.spawn(f"{myTerm} --exec=htop")),
    
    # Toggle between different layouts as defined below
    Key([mod], "Tab", lazy.next_layout()),
    Key([mod], "q", lazy.window.kill()),
    
    # Toggle Qtile running
    Key([mod, "control"], "r", lazy.restart()),
    Key([mod, "control"], "q", lazy.shutdown()),
    
    # Laptop 'Fn'-type keys
    Key([], "Print", lazy.spawn("flameshot gui")),
    Key([], "XF86AudioMicMute", lazy.spawn("amixer set Capture toggle")),
    Key([], "XF86AudioPlay", lazy.spawn("moc --toggle-pause")),
    Key([], "XF86AudioPrev", lazy.spawn("moc --previous")),
    Key([], "XF86AudioNext", lazy.spawn("moc --next")),
    # Key([], "XF86ScreenSaver", lazy.spawn("slock"))
    # Key([], "XF86Lock", lazy.cmd("hibernate"))
]

group_names = 'WWW DEV SYS MISC'.split()

groups = [Group(name, layout='max') for name in group_names]

for i, name in enumerate(group_names):
    indx = str(i+1)
    keys.extend([
        # mod + group number = switch to group
        Key([mod], indx, lazy.group[name].toscreen()),
        # mod1 + shift + group number = switch to & move focused window to group
        Key([mod, "shift"], indx, lazy.window.togroup(name, switch_group=True)),
    ])

#Define a bunch of colours to use mate

# colours = [['#5d4432','#5d4432'], # panel background
#            ['#f7bf7a','#f7bf7a'], # font colour
#            ['#d27c02','#d27c02'], # main accent colour
#            ['#ff5601','#ff5601'], # exit colour
#            ['#9b9e17','#9b9e17']] # additional accent colour
col_bg = '#5d4432'
col_fnt = '#f7bf7a'
col_acct1 = '#d27c02'
col_acct2 = '#9b9e17'
col_warn = '#ff5601'

# Available layouts

layouts = [
    layout.Max(),
    layout.Stack(
        num_stacks=2,
        border_focus=col_acct2,
        border_width=2
        ),
    # layout.Bsp(),
    # layout.Columns(),
    # layout.Matrix(),
     layout.MonadTall(
         border_focus=col_acct2,
         ),
    # layout.MonadWide(),
    # layout.RatioTile(),
    # layout.Tile(),
    # layout.TreeTab(),
    # layout.VerticalTile(),
    # layout.Zoomy(),
]

widget_defaults = dict(
    font='CaskaydiaCove Nerd Font',
    fontsize=16,
    padding=6,
    background=col_bg,
    foreground=col_fnt,
)
extension_defaults = widget_defaults.copy()


screens = [
    Screen(
        top=bar.Bar(
            [
                widget.CurrentLayout(
                    width=10*len('MonadTall'),
                    font='UbuntuMono',
                    padding=10,
                    background=col_acct2,
                    foreground=col_fnt,
                    ),
                widget.GroupBox(
		    font='UbuntuMono',
                    active=col_bg,
                    this_current_screen_border=col_acct1,
                    hide_unused=True,
                    highlight_method='block',
                    padding=10,
                    background=col_acct1,
                    foreground=col_fnt,
                    ),
                widget.Prompt(
                    ),
                widget.Spacer(
                    ),
                widget.Moc(
                    ),
                widget.TextBox(
                    text='',
                    padding=6,
                    ),
                widget.Volume(
                    #fmt='{ 0}',
                    padding=5,
                    ),
                widget.Sep(
                    padding=1
                    ),
                #widget.TextBox(
                    #text='<',
                    #fontsize=10
                    #),
                widget.TextBox(
                    text='',
                    padding=6,
                    ),
                widget.Backlight(
                    backlight_name='intel_backlight',
                    brightness_file='brightness',
                    change_command='light -S {0}',
                    padding=2,
                    ),
                widget.Sep(
                    padding=1
                    ),
                widget.Battery(
                    charge_char='',
                    discharge_char=' ',
                    full_char=' ',
                    unknown_char='??',
                    format='{char} {percent:2.0%} {hour:d}:{min:02d}',
                    low_foreground=col_warn,
                    low_percentage=0.25,
                    notify_below=0.3,
                    padding=6
                    ),
                widget.Sep(
                    padding=1
                    ),
                widget.CheckUpdates(
                    display_format=' {updates}',
                    colour_have_updates=col_acct1,
                    colour_no_updates=col_warn,
                    update_interval=3600,
                    ),
                # widget.Sep(
                #     padding=1
                #     ),
                # widget.Wlan(
                #     disconnected_message='No Wi-Fi! :(',
                #     background=col_warn,
                #     ),
                widget.Systray(
                    icon_size=15,
                    padding=5
                    ),
                widget.Sep(
                    padding=1
                    ),
                widget.TextBox(
                    text='',
                    ),
                widget.Clock(
                    format='%H:%M %d-%m',
                    ),
                widget.QuickExit(
                    background=col_warn,
                    foreground=col_bg,
                    default_text=' Exit qtile',
                    countdown_format='  {}s...     ',
                    countdown_start=4
                    ), 
            ],
            24,
        ),
    ),
]

# Drag floating layouts.
mouse = [
    Drag([mod], "Button1", lazy.window.set_position_floating(),
         start=lazy.window.get_position()),
    Drag([mod], "Button3", lazy.window.set_size_floating(),
         start=lazy.window.get_size()),
    Click([mod], "Button2", lazy.window.bring_to_front())
]

dgroups_key_binder = None
dgroups_app_rules = []  # type: List
main = None
follow_mouse_focus = True
bring_front_click = False
cursor_warp = False
floating_layout = layout.Floating(float_rules=[
    # Run the utility of `xprop` to see the wm class and name of an X client.
    {'wmclass': 'confirm'},
    {'wmclass': 'dialog'},
    {'wmclass': 'download'},
    {'wmclass': 'error'},
    {'wmclass': 'file_progress'},
    {'wmclass': 'notification'},
    {'wmclass': 'splash'},
    {'wmclass': 'toolbar'},
    {'wmclass': 'confirmreset'},  # gitk
    {'wmclass': 'makebranch'},  # gitk
    {'wmclass': 'maketag'},  # gitk
    {'wname': 'branchdialog'},  # gitk
    {'wname': 'pinentry'},  # GPG key password entry
    {'wmclass': 'ssh-askpass'},  # ssh-askpass
])
auto_fullscreen = True
focus_on_window_activation = "smart"

# STARTUP APPS
@hook.subscribe.startup_once

def autostart():
    home = os.path.expanduser('~')
    subprocess.Popen([home + '/.config/qtile/autostart.sh'])

# XXX: Gasp! We're lying here. In fact, nobody really uses or cares about this
# string besides java UI toolkits; you can see several discussions on the
# mailing lists, GitHub issues, and other WM documentation that suggest setting
# this string if your java app doesn't work correctly. We may as well just lie
# and say that we're a working one by default.
#
# We choose LG3D to maximize irony: it is a 3D non-reparenting WM written in
# java that happens to be on java's whitelist.
wmname = "LG3D"
